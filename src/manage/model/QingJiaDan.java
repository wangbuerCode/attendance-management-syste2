package manage.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_qingjiadan")
public class QingJiaDan {

	private int id;
	
	private Date createtime;
	
	private String  qjtime1;//��ٿ�ʼʱ��
	
	private String  qjtime2;//��ٽ���ʱ��
	
	private String codenum;//ѧ�����
	
	private String username;//����
	
	private String banjinum;//�༶���
	
	private String shenhe;//���״̬
	
	private String shenhecontent;//������

	@Id
	@GeneratedValue 
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public String getQjtime1() {
		return qjtime1;
	}

	public void setQjtime1(String qjtime1) {
		this.qjtime1 = qjtime1;
	}

	public String getQjtime2() {
		return qjtime2;
	}

	public void setQjtime2(String qjtime2) {
		this.qjtime2 = qjtime2;
	}

	public String getCodenum() {
		return codenum;
	}

	public void setCodenum(String codenum) {
		this.codenum = codenum;
	}

	public String getShenhe() {
		return shenhe;
	}

	public void setShenhe(String shenhe) {
		this.shenhe = shenhe;
	}

	public String getShenhecontent() {
		return shenhecontent;
	}

	public void setShenhecontent(String shenhecontent) {
		this.shenhecontent = shenhecontent;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getBanjinum() {
		return banjinum;
	}

	public void setBanjinum(String banjinum) {
		this.banjinum = banjinum;
	}

}
